import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Calc from '@/components/Calc'
import VueNoty from 'vuejs-noty'

Vue.use(VueNoty)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/calc',
      name: 'calc',
      component: Calc
    }
  ]
})
